# Clone du projet

    git clone https://gitlab.com/azarias1270/petit-amazon-svelte.git


# Installation des modules et lancement de l'application

install dependencies

    npm install

serve with hot reload at localhost:8080

    npm run dev
